﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bivaa_server_main.Core
{
    public interface IStudentService
    {
        List<student> GetAllStudents();
        student GetStudentById(int id);
        student CreateStudent(student student);
        student UpdateStudent(int id, student student);
        void DeleteStudent(int id);
    }
}