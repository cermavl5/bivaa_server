using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace bivaa_server_main
{
    public partial class OnlineSchoolDbContext : DbContext
    {
        public OnlineSchoolDbContext()
            : base("name=OnlineSchoolDbContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<classroom> classroom { get; set; }
        public virtual DbSet<student> student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<classroom>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .Property(e => e.classroom)
                .IsUnicode(false);
        }
    }
}
