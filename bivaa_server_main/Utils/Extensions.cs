﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bivaa_server_main.Utils
{
    public static class Extensions
    {
        public static string toJson(this object obj) => JsonConvert.SerializeObject(obj);

        public static string toJson(this student student)
        {
            dynamic result = new JObject();
            result.Id = student.id;
            result.Code = student.name;
            result.Name = student.email;
            result.Rate = student.classroom;
            return Convert.ToString(result);
        }

        public static T fromJson<T>(this string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }

        public static void ApplyJsonContentType(this HttpResponseMessage msg, string mediaType = "application/json")
        {
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(mediaType);
        }
    }
}