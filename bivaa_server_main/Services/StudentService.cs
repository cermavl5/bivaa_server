﻿using bivaa_server_main.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server_main.Services
{
    public class StudentService : IStudentService
    {
        public List<student> GetAllStudents()
        {
            using (var db = new OnlineSchoolDbContext())
            {
                var students = (from student in db.student select student).ToList();
                return students;
            }
        }

        public student GetStudentById(int id)
        {
            using (var db = new OnlineSchoolDbContext())
            {
                var student = (from student in db.student where student.id == id select student).SingleOrDefault();
                return student;
            }
        }
        public student CreateStudent(student student)
        {
            using (var db = new OnlineSchoolDbContext())
            {
                db.student.Add(student);
                db.SaveChanges();
                return student;
            }
        }

        public void DeleteStudent(int id)
        {
            using (var db = new OnlineSchoolDbContext())
            {
                var student = new student { id = id };
                db.student.Attach(student);
                db.student.Remove(student);
                db.SaveChanges();
            }
        }

        public student UpdateStudent(int id, student requestRate)
        {
            var student = GetStudentById(id);
            if (student != null)
            {
                using (var db = new OnlineSchoolDbContext())
                {
                    student.name = requestRate.name;
                    student.email = requestRate.email;
                    student.classroom = requestRate.classroom;
                    db.SaveChanges();
                }
                return student;
            }
            else
            {
                // log error
                return null;
            }
        }
    }
}