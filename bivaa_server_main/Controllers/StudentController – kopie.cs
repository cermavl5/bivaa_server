﻿//using bivaa_server_main.Core;
//using bivaa_server_main.Utils;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Web;
//using System.Web.Http;
//using System.Web.Cors;
//using System.Web.Http.Cors;

//namespace bivaa_server_main.Controllers
//{
//    [EnableCors(origins: "*", headers: "*", methods: "*")]

//    public class StudentController : BaseController
//    {
//        private IStudentService studentService;
//        public StudentController(ICommonService commonService, IStudentService studentService) : base(commonService)
//        {
//            this.studentService = studentService;
//        }

//        [HttpGet]
//        [ActionName("all")]
//        public HttpResponseMessage GetAll()
//        {
//            using (var db = new OnlineSchoolDbContext())
//            {
//                var students = (from stud in db.student select stud).ToList();

//                var jsonString = JsonConvert.SerializeObject(students);

//                var result = new HttpResponseMessage()
//                {
//                    Content = new StringContent(jsonString)
//                };
//                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

//                return result;
//            }
//            //var students = studentService.GetAllStudents();
//            //return commonService.GetResponse(students.toJson());
//        }

//        [HttpGet]
//        [ActionName("detail")]
//        public HttpResponseMessage GetDetail(int id)
//        {
//            var student = studentService.GetStudentById(id);
//            return commonService.GetResponse(student.toJson());
//        }

//        [HttpPost]
//        [ActionName("create")]
//        public HttpResponseMessage Create(HttpRequestMessage req)
//        {
//            var requestBody = req.Content.ReadAsStringAsync().Result;
//            var requestStudent = requestBody.fromJson<student>();
//            var createdStudent = studentService.CreateStudent(requestStudent);
//            return commonService.GetResponse(createdStudent.toJson());
//        }

//        [HttpPut]
//        [ActionName("update")]
//        public HttpResponseMessage Update(int id, HttpRequestMessage req)
//        {
//            var requestBody = req.Content.ReadAsStringAsync().Result;
//            var requestStudent = requestBody.fromJson<student>();

//            // create obj in database - service call
//            var updatedStudent = studentService.UpdateStudent(id, requestStudent);

//            // return created object
//            return commonService.GetResponse(updatedStudent.toJson());
//        }

//        [HttpDelete]
//        [ActionName("delete")]
//        public HttpResponseMessage Delete(int id)
//        {
//            studentService.DeleteStudent(id);
//            return commonService.GetResponse();
//        }
//    }

//}